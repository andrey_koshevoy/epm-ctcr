CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation
 
 
 INTRODUCTION
---------------------

This is a cat clicker project that was developed during the angularJS training in EPAM systems.
 

REQUIREMENTS
------------
```javascript
{ 
    "angular": "~1.3.16",
    "angular-route": "~1.4.0",
    "angular-loader": "~1.4.0",
    "angular-mocks": "~1.4.0",
    "html5-boilerplate": "~5.2.0",
    "bootstrap": "~3.3.5",
    "fotorama": "~4.6.4",
    "angular-ui-router": "~0.2.15",
    "fontawesome": "~4.2.0",
    "underscore": "~1.8.3",
    "angular-ui-grid": "~3.0.1",
    "angular-daterangepicker": "~0.1.16",
    "angular-ui-select": "~0.12.0",
    "angular-messages": "~1.4.3"
}
```
INSTALLATION
------------
 * Install
    
   1. npm install
   2. bower install
   3. npm start

