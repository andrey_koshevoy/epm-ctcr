This is a cat clicker app.

# What this app about #

### Step 1 ###
Visuals
● The application should display a picture of a cat and a number of clicks.
● The specifics of the layout do not matter, so style it however you'd like.
Interaction
● The number of clicks should increment when the cat picture is clicked.

### Step 2 ###
Visuals
● The application should display two cats. Each cat includes
o the cat's name
o a picture of the cat
o text showing the number of clicks
● The specifics of the layout do not matter, so style it however you'd like.
Interaction
● The number of clicks should increment when each cat picture is clicked.

### Step 3 ###
Visuals
● The application should display
o a list of at least 5 cats, listed by name
o an area to display the selected cat
● In the cat display area, the following should be displayed
o the cat's name
o a picture of the cat
o text showing the number of clicks
● The specifics of the layout do not matter, so style it however you'd like.
Interaction
● When a cat name is clicked in the list, the cat display area should update to show the data
for the selected cat.
● The number of clicks in the cat area should be unique to each cat, and should increment
when the cat's picture is clicked.

### Step 4 ###
Visuals
● The application should display
o a list of at least 5 cats, listed by name
o an area to display the selected cat
● If the image of a cat was already viewed, you should display a tick sign ✓ next to the cat’s
name.
● In the cat display area, the following should be displayed
o the cat's name
o a picture of the cat
o vote spinner
● The user should not be able to see a flash of unbound interpolation.
Interaction
● The votes should increment or decrement depending on whether up or down arrow was
clicked in the spinner.

### Step 5 ###
Visuals
● Add a search box to find cats by name with a button.
● Add a dropdown to choose order of the cats.
Interaction
● The cats filtering should be applied after the button next to the search field is clicked.
● Ascending or descending order should be applied immediately after the selected value in
orderBy dropdown is changed.

### Step 6 ###
Visuals
● The form should display input fields to modify the following:
o the cat's name
o an url of a cat’s picture
o an image from the picture’s url
● Visual feedback about validity of the forms and corresponding error messages.
● The specifics of the layout do not matter, so style it however you'd like.
Interaction
● The form should have 2 buttons to save or cancel editing (saving is allowed not to work for
now).
● The form should contain validation rules: both fields are required, url should be a valid url. If
some of the rules does not match, user won’t be able to click save button.
● The url change should be reflected in the model after blur.