var expressIO = require('express.io');

var app = expressIO();

app.use(expressIO.cookieParser());
app.use(expressIO.session({secret: 'monkey'}));

app.http().io();
app.listen(8000);

// Session is automatically setup on initial request.
app.get('/', function(req, res) {
    req.session.loginDate = new Date().toString();
    res.sendfile(__dirname + '/app/Views/index.html');
});

app.use(expressIO.static(__dirname + '/'));
app.use(expressIO.static(__dirname + '/app/'));


exports = module.exports = app;


