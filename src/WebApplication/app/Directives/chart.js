/**
 * Created by Andrei_Kashavy on 7/17/2015.
 */
(function () {
    app.directive("catChart", ["$log", "$timeout", function ($log, $timeout) {
        return {
            restrict: "E",
            require: "ngModel",
            templateUrl: "../app/Views/Templates/chartTmp.html",
            scope: {
                name: "@",
                data: "=ngModel"
            },
            controller: function ($scope) {

                var drawChart = function () {
                    var barData = $scope.data;
                    var elementName = '#visualisation-' + $scope.name + '';
                    var vis = d3.select(elementName),
                        WIDTH = 500,
                        HEIGHT = 250,
                        MARGINS = {
                            top: 20,
                            right: 20,
                            bottom: 20,
                            left: 50
                        },
                        xRange = d3.scale.ordinal()
                            .rangeRoundBands([MARGINS.left, WIDTH - MARGINS.right], 0.1)
                            .domain(barData.map(function (d) {
                                return d.name;
                            })),


                        yRange = d3.scale.linear().range([HEIGHT - MARGINS.top, MARGINS.bottom]).domain([0,
                            d3.max(barData, function (d) {
                                return d.count;
                            })
                        ]),

                        xAxis = d3.svg.axis()
                            .scale(xRange)
                            .tickSize(5)
                            .tickSubdivide(true),

                        yAxis = d3.svg.axis()
                            .scale(yRange)
                            .tickSize(5)
                            .orient("left")
                            .tickSubdivide(true);


                    vis.append('svg:g')
                        .attr('class', 'x axis')
                        .attr('transform', 'translate(0,' + (HEIGHT - MARGINS.bottom) + ')')
                        .call(xAxis);

                    vis.append('svg:g')
                        .attr('class', 'y axis')
                        .attr('transform', 'translate(' + (MARGINS.left) + ',0)')
                        .call(yAxis);

                    vis.selectAll('rect')
                        .data(barData)
                        .enter()
                        .append('rect')
                        .attr('x', function (d) {
                            return xRange(d.name);
                        })
                        .attr('y', function (d) {
                            return yRange(d.count);
                        })
                        .attr('width', xRange.rangeBand())
                        .attr('height', function (d) {
                            return ((HEIGHT - MARGINS.bottom) - yRange(d.count));
                        })
                        .attr('fill', 'grey');
                };
                $timeout(drawChart, 0);
            }
        };
    }]);

})();