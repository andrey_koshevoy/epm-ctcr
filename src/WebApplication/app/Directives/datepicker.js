/**
 * Created by Andrei_Kashavy on 7/20/2015.
 */
(function (){
    app.directive('catDate',[ '$log','$timeout', function($log, $timeout)
    {
        return{
            restrict: "E",
            require: "ngModel",
            template:"<input type='text' id='{{selector}}'  class='{{cls}}' placeholder='DD-MM-YYYY' required readonly='readonly' />",
            scope: {
                data: "=ngModel",
                selector: "@selector",
                cls: "@cls"
            },
            link: function(scope, element, attrs)
            {
                var init =  function() {
                    $('#'+scope.selector).daterangepicker({
                            singleDatePicker: true,
                            showDropdowns: true
                        },
                        function(start) {
                           scope.data = start.format('DD-MM-YYYY');
                        });
                };
                $timeout(init, 0);
            }
        }
    }])
})();