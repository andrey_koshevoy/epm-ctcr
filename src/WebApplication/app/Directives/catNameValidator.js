/**
 * Created by Andrei_Kashavy on 7/20/2015.
 */
(function () {
    app.directive('catNameValidator', function ($q, CatService, $timeout) {
        return {
            require: 'ngModel',
            link: function (scope, element, attrs, ngModel) {
                ngModel.$asyncValidators.name = function (modelValue, viewValue) {
                    if (!viewValue) {
                        return $q.when(true);
                    }
                    var deferred = $q.defer();
                    $timeout(function () {
                        if (viewValue && _.find(CatService.GetCats(), function (cat) {
                                return cat.name === viewValue;
                            }) != undefined) {
                            deferred.reject();
                        }

                        deferred.resolve();
                    }, 1000);

                    return deferred.promise;
                };
            }
        };
    })
})();