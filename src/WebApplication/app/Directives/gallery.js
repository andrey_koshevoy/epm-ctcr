/**
 * Created by Andrei_Kashavy on 7/20/2015.
 */
(function (){
    app.directive('catGallery',[ '$log', function($log)
    {
        return{
            restrict: "E",
            require: "ngModel",
            templateUrl: "../app/Views/Templates/gallery.html",
            scope: {
                data: "=ngModel"
            },
            link: function(scope, element, attrs)
            {
                var $fotoramaDiv = $('#fotorama').fotorama();
                var fotorama = $fotoramaDiv.data('fotorama');
                scope.$watch('data', function() {
                        fotorama.load(scope.data);
                    },true
                );
            }
        }
    }])
})();