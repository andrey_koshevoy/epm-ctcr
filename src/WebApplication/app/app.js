'use strict';

// Declare app level module which depends on views, and components
var app=angular.module('application', [
    //'ngRoute'
    'ui.router',
    'ui.grid',
    'ui.select',
    'ui.grid.selection',
    'ui.grid.autoResize',
    'ngMessages'
]);