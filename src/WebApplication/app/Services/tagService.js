/**
 * Created by Andrei_Kashavy on 7/15/2015.
 */
(function() {
    'use strict';
    app.factory('TagService',['CatService', '$log', TagService]);

    function TagService(CatService, $log) {

        var tagSRV = {};

        tagSRV.getTags = function()
        {
            var merged = [];
            _.each(CatService.GetCats(), function(cat)
            {
                $log.info(cat);
                merged = _.union(merged, cat.tags);
                $log.info(merged);
            });

            return merged;
        };

        return tagSRV;
    };
})();
