/**
 * Created by Andrei_Kashavy on 7/15/2015.
 */
(function() {
    'use strict';
    app.factory('CatService', CatService);

    function CatService() {

        var fc = {};

        fc.cats = [
            {
                name: "John",
                imgUrl: "http://www.jeremynoeljohnson.com/wp-content/uploads/2014/06/crazy_cat_2.jpg",
                clicks: 1,
                votes: 3,
                isViewed: false,
                birthday: '10-10-2010',
                tags:[]
            },
            {
                name: "Joann",
                imgUrl: "https://pbs.twimg.com/profile_images/424484505915621376/EOwsjaMZ_400x400.png",
                clicks: 5,
                votes: 1,
                isViewed: false,
                birthday: '05-05-2005',
                tags:[]
            },
            {
                name: "Mike",
                imgUrl: "http://www.shareably.net/wp-content/uploads/2015/01/unique_cat_fur_1.jpg",
                clicks: 10,
                votes: 9,
                isViewed: false,
                birthday: '21-01-1994',
                tags:[]
            },
            {
                name: "Paul",
                imgUrl: "http://www.kittenswhiskers.com/wp-content/uploads/sites/48/2014/01/white-cat-blue-eyes-640x360.jpg",
                clicks: 4,
                votes: 16,
                isViewed: false,
                birthday: '10-07-2008',
                tags:['Bad']
            },
            {
                name: "Obama",
                imgUrl: "http://www.davidmcelroy.org/wp-content/uploads/2011/08/Dagny-upset.jpg",
                clicks: 1,
                votes: 0,
                isViewed: false,
                birthday: '10-10-2011',
                tags:['Black', "Cool"]
            },
            {
                name: "Begemot",
                imgUrl: "https://f-a.d-cd.net/cd8e664s-960.jpg",
                clicks: 6,
                votes: 15,
                isViewed: false,
                birthday: '10-10-2015',
                tags:['Awesome']
            },
        ];
        fc.GetCats = GetCats;
        fc.AddCat = AddCat;

        function GetCats() {
            return this.cats;
        };

        function AddCat(cat) {
            this.cats.push(cat);
        }

        return fc;
    };
})();
