/**
 * Created by Andrei_Kashavy on 7/16/2015.
 */
(function () {
    app.config(function ($routeProvider) {
        $routeProvider

            // route for the home page
            .when('/cats', {
                templateUrl: '../app/Views/listOfCats.html',
                controller: 'catCtrl'
            })
            // route for the about page
            .when('/cats/addOrUpdate', {
                templateUrl: '../app/Views/addOrUpdateCat.html',
                controller: 'addOrUpdateCtrl'
            })
            .when('/cats/popularity', {
                templateUrl: '../app/Views/catsPopularity.html',
                controller: 'popularityCtrl'
            })
            .when('/cats/gallery', {
                templateUrl: '../app/Views/catsGallery.html',
                controller: 'galleryCatController'
            })
            .
            otherwise({
                redirectTo: '/cats'
            });
    });
})();
