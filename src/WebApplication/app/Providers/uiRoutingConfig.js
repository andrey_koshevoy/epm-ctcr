/**
 * Created by Andrei_Kashavy on 7/16/2015.
 */
(function () {
    app.config(function ($urlRouterProvider, $stateProvider) {
        $stateProvider

            // route for the home page
            .state('cats', {
                url: "/cats",
                templateUrl: '../app/Views/listOfCats.html',
                controller: 'catCtrl'
            })
            // route for the about page
            .state('addOrUpdate', {
                url: '/cats/addOrUpdate',
                templateUrl: '../app/Views/addOrUpdateCat.html',
                controller: 'addOrUpdateCtrl'
            })
            .state('popularity', {
                url: '/cats/popularity',
                templateUrl: '../app/Views/catsPopularity.html',
                controller: 'popularityCtrl'
            })
            .state('gallery', {
                url: '/cats/gallery',
                templateUrl: '../app/Views/catsGallery.html',
                controller: 'galleryCatController'
            });

        $urlRouterProvider.otherwise("/cats");
    });
})();
