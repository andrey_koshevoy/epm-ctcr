/**
 * Created by Andrei_Kashavy on 7/20/2015.
 */
(function(){
    app.controller('galleryCatController', function($scope,CatService)
    {
        $scope.data = _.map(CatService.GetCats(), function(cat){
            return {url:cat.imgUrl, thumb: cat.imgUrl};
        });
    })
})();
