/**
 * Created by Andrei_Kashavy on 7/15/2015.
 */
(function () {
    'use strict';

    app.controller('popularityCtrl', ["$scope", "CatService", "$log", function ($scope, CatService, $log) {
        $scope.clicks = [];
        $scope.votes = [];
        $scope.initStats = function()
        {
            var localClicks = [];
            var localVotes = [];
            angular.forEach($scope.cats, function(cat)
            {
                var clck = {name:cat.name, count:cat.clicks};
                var vts = {name:cat.name, count:cat.votes};
                localClicks.push(clck);
                localVotes.push(vts);
            });
            $scope.clicks= localClicks;
            $scope.votes= localVotes;
        };
        $scope.cats = CatService.GetCats();
    }]);
})();
