/**
 * Created by Andrei_Kashavy on 7/15/2015.
 */
(function () {
    'use strict';

    app.controller('addOrUpdateCtrl', function ($state, $scope, CatService, TagService) {
        $scope.cat = {
            clicks: 0,
            votes: 0,
            isViewed: false,
            birthday: '',
            tags: []
        };
        $scope.oldCat = angular.copy($scope.cat);

        $scope.reset = function () {
            $scope.cat = angular.copy($scope.oldCat);
            goHome();
        }

        $scope.save = function () {
            CatService.AddCat($scope.cat);
            window.alert("Cat has benn saved");
            goHome();
        }

        $scope.tags = TagService.getTags();

        var goHome = function()
        {
            $state.go('cats');
        };

    });
})();
