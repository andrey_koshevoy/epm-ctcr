/**
 * Created by Andrei_Kashavy on 7/14/2015.
 */
(function () {

    'use strict';
    app.controller('catCtrl', function ($log, $scope, CatService) {

        $scope.currentCat = null;

        $scope.catSelected = false;

        $scope.sortMode = "Descending";

        $scope.cats = CatService.GetCats();

        $scope.clickOnTheCat = function (cat) {
            cat.clicks++;
        }

        $scope.voteUpTheCat = function (cat) {
            cat.votes++;
        }

        $scope.voteDownTheCat = function (cat) {
            cat.votes--;
        }

        $scope.selectCat = function (cat) {
            if (cat.isViewed == false) {
                cat.isViewed = true;
            }
            $scope.currentCat = cat;
            $scope.catSelected = true;

        }

        $scope.orderByMode = function () {
            if ($scope.sortMode == "Descending") {
                return "-name";
            }
            else {
                return "name";
            }
        }

        $scope.gridOptions = {
            rowHeight: 30,
            data: $scope.cats,
            showFooter: false,
            enableSorting: true,
            multiSelect: false,
            enableFiltering: true,
            enableRowSelection: true,
            enableSelectAll: false,
            enableRowHeaderSelection: false,
            noUnselect: true,
            onRegisterApi: function (gridApi) {
                $scope.gridApi = gridApi;
                gridApi.selection.on.rowSelectionChanged($scope, function(row){
                    $log.info(row);
                    $scope.selectCat(row.entity);
                })
            },
            columnDefs: [
                {Name: 'name', field: 'name'},
                {
                    Name: 'isViewed',
                    field: 'isViewed',
                    enableFiltering: false,
                    cellTemplate: '<img ng-show="row.entity.isViewed" src="../Assets/Images/checkmark.png" class="img-thumbnail" alt="check" width="20" height="20">'
                },
                {Name: 'clicks', field: 'clicks', enableFiltering: false},
                {Name: 'votes', field: 'votes', enableFiltering: false}],
            rowTemplate: ''
        }

        $scope.getTableHeight = function() {
            var rowHeight = 30; // your row height
            var headerHeight = 80; // your header height
            return {
                height: ($scope.gridOptions.data.length * rowHeight + headerHeight) + "px"
            };
        };


    });
})();
