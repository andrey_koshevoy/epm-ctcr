﻿namespace CTCR.API.Core
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;

    using CTCR.API.Models;

    /// <summary>
    /// The cat repository.
    /// </summary>
    public class CatRepository : ICatRepository
    {
        /// <summary>
        /// The local cats.
        /// </summary>
        private static readonly IList<Cat> LocalCats = new List<Cat>()
                                                   {
                                                       new Cat { Name = "Tome", ImageURL = "http://www.jeremynoeljohnson.com/wp-content/uploads/2014/06/crazy_cat_2.jpg" },
                                                       new Cat { Name = "Jhone", ImageURL = "https://pbs.twimg.com/profile_images/424484505915621376/EOwsjaMZ_400x400.png" },
                                                       new Cat { Name = "Obama", ImageURL = "http://www.davidmcelroy.org/wp-content/uploads/2011/08/Dagny-upset.jpg" },
                                                       new Cat { Name = "Begemot", ImageURL = "https://f-a.d-cd.net/cd8e664s-960.jpg" },
                                                       new Cat { Name = "Theodore", ImageURL = "http://www.shareably.net/wp-content/uploads/2015/01/unique_cat_fur_1.jpg" },
                                                       new Cat { Name = "Mike", ImageURL = "http://www.isanet.org/Portals/0/Users/001/81/58881/black-cat-4.jpg" },
                                                   };

        /// <summary>
        /// Gets the cats.
        /// </summary>
        public IEnumerable<Cat> Cats
        {
            get
            {
                return LocalCats;
            }
        }

        /// <summary>
        /// The add.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        public void Add(Cat entity)
        {
            LocalCats.Add(entity);
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        public void Delete(Cat entity)
        {
            LocalCats.Remove(entity);
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <exception cref="DataException">
        /// </exception>
        public void Update(Cat entity)
        {
            var local = LocalCats.FirstOrDefault(ct => ct.Id == entity.Id);
            if (local != null)
            {
                local.Name = entity.Name;
                local.ImageURL = entity.ImageURL;
                local.Clicks = entity.Clicks;
                local.Votes = entity.Clicks;
            }
            else
            {
                throw new DataException("Object not found");
            }
        }

        /// <summary>
        /// The find by id.
        /// </summary>
        /// <param name="Id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Cat"/>.
        /// </returns>
        public Cat FindById(Guid Id)
        {
           return LocalCats.FirstOrDefault(ct => ct.Id == Id);
        }
    }
}