﻿namespace CTCR.API.Core
{
    using System;
    using System.Collections.Generic;

    using CTCR.API.Models;

    /// <summary>
    /// The CatRepository interface.
    /// </summary>
    public interface ICatRepository
    {
        /// <summary>
        /// Gets the list.
        /// </summary>
        IEnumerable<Cat> Cats { get; }

        /// <summary>
        /// The add.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        void Add(Cat entity);

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        void Delete(Cat entity);

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        void Update(Cat entity);

        /// <summary>
        /// The find by id.
        /// </summary>
        /// <param name="Id">
        ///     The id.
        /// </param>
        /// <returns>
        /// The <see cref="Cat"/>.
        /// </returns>
        Cat FindById(Guid Id);
    }
}
