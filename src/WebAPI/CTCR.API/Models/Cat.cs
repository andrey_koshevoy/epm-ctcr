﻿
namespace CTCR.API.Models
{
    using System;

    /// <summary>
    /// The cat.
    /// </summary>
    public class Cat
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Cat"/> class.
        /// </summary>
        public Cat()
        {
            this.Id = Guid.NewGuid();
        }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the image url.
        /// </summary>
        public string ImageURL { get; set; }

        /// <summary>
        /// Gets or sets the clicks.
        /// </summary>
        public int Clicks { get; set; }

        /// <summary>
        /// Gets or sets the votes.
        /// </summary>
        public int Votes { get; set; }
    }
}