﻿
namespace CTCR.API.Controllers
{
    using System.Collections.Generic;
    using System.Web.Http;

    using CTCR.API.Core;
    using CTCR.API.Models;

    /// <summary>
    /// The cat controller.
    /// </summary>
    public class CatController : ApiController
    {
        /// <summary>
        /// The _cat repository.
        /// </summary>
        private readonly ICatRepository _catRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="CatController"/> class.
        /// </summary>
        /// <param name="catRepository">
        /// The cat repository.
        /// </param>
        public CatController(ICatRepository catRepository)
        {
            this._catRepository = catRepository;
        }

        // GET api/<controller>
        public IEnumerable<Cat> Get()
        {
            return _catRepository.Cats;
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}