﻿namespace CTCR.API
{
    using Microsoft.Practices.Unity;

    public interface IContainerAccessor
    {
        IUnityContainer Container { get; }
    }
}
